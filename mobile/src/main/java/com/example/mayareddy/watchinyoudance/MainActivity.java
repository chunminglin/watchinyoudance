package com.example.mayareddy.watchinyoudance;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends Activity {

    private static final int CAMERA_REQUEST_CODE = 0;
    // private static final int LIBRARY_KITKAT_REQUEST_CODE = 1;
    private static final int LIBRARY_REQUEST_CODE = 2;

    private static final String PHOTO_FILENAME = "photo.png";

    RoundImageView headPhoto;
    ImageButton cameraButton;
    ImageButton libraryButton;

    String absolutePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        // initialize the head photo
        headPhoto = (RoundImageView)findViewById(R.id.id_head_imageview);
        absolutePath = getApplicationContext().getFilesDir().getAbsolutePath();
        File file = new File(absolutePath + "/" + PHOTO_FILENAME);
        if(file.exists())
        {
            Bitmap bm = BitmapFactory.decodeFile(absolutePath + "/" + PHOTO_FILENAME);
            headPhoto.setImageBitmap(bm);
        }
        else
        {
            headPhoto.setImageDrawable(getResources().getDrawable(R.drawable.default_photo));
        }

        // initialize the imagebuttons
        cameraButton = (ImageButton)findViewById(R.id.id_camera_imagebutton);
        libraryButton = (ImageButton)findViewById(R.id.id_library_imageButton);
        cameraButton.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        intentCamera();
                    }
                }
        );
        libraryButton.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        intentLibrary();
                    }
                }
        );
    }

    private void intentLibrary() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);//ACTION_OPEN_DOCUMENT
            intent.setType("image/*");
            startActivityForResult(intent, LIBRARY_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void intentCamera(){
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode!=RESULT_OK)
        {
            headPhoto.setImageDrawable(getResources().getDrawable(R.drawable.default_photo));
            return;
        }
        if(requestCode==CAMERA_REQUEST_CODE && saveImgFromCamera(data)) {
            Bitmap bm = BitmapFactory.decodeFile(absolutePath + "/" + PHOTO_FILENAME);
            headPhoto.setImageBitmap(bm);
        }else if(requestCode==LIBRARY_REQUEST_CODE && saveImgFromLibrary(data) ){
            Bitmap bm = BitmapFactory.decodeFile(absolutePath + "/" + PHOTO_FILENAME);
            headPhoto.setImageBitmap(bm);
        }
    }

    private boolean saveImgFromCamera(Intent data){
        Bundle bundle = data.getExtras();
        Bitmap bm = (Bitmap) bundle.get("data");

        if (bm != null){
            bm.recycle();
            bm = (Bitmap) data.getExtras().get("data");
            try {
                File file = new File(absolutePath + "/" + PHOTO_FILENAME);
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
                return true;
            }catch(IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean saveImgFromLibrary(Intent data){
        Uri uri = data.getData();
        try {
            Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            File file = new File(absolutePath + "/" + PHOTO_FILENAME);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            return true;
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
